package com.pluralsight;

import com.pluralsight.model.Activity;
import com.pluralsight.model.ActivitySearch;
import com.pluralsight.repository.ActivityRepository;
import com.pluralsight.repository.ActivityRepositoryStub;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("search/activities")
public class ActivitySearchResource {
    private static final Logger logger = LoggerFactory.getLogger(ActivitySearchResource.class);

    private ActivityRepository activityRepository = new ActivityRepositoryStub();

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response searchForActivities(@QueryParam(value = "description") List<String> descriptions) {
        logger.info("Looking for descriptions: {}", descriptions);

        List<Activity> activities = activityRepository.findByDescriptions(descriptions);

        if (activities == null || activities.size() == 0) {
            logger.info("Returning 404.");
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        logger.info("Retuning activities: {}", activities);

        return Response.ok(new GenericEntity<List<Activity>>(activities) {}).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response searchForActivitiesUsingCriteria(ActivitySearch criteria) {
        logger.info("Searching using criteria: {}", criteria);

        List<Activity> found = activityRepository.findByCriteria(criteria);
        if (found == null || found.size() == 0) {
            logger.info("Returning 404.");
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        logger.info("Retuning activities: {}", found);

        return Response.ok(new GenericEntity<List<Activity>>(found) {}).build();
    }
}
