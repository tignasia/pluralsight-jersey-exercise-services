package com.pluralsight;

import com.pluralsight.model.Activity;
import com.pluralsight.model.User;
import com.pluralsight.repository.ActivityRepository;
import com.pluralsight.repository.ActivityRepositoryStub;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("activities")
public class ActivityResource {
    private static final Logger logger = LoggerFactory.getLogger(ActivityResource.class);

    private ActivityRepository activityRepository = new ActivityRepositoryStub();

    @GET
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public List<Activity> getAllActivities() {
        logger.info("Returning list of all activities.");
        return activityRepository.findAllActivities();
    }

    @GET
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    @Path("{activityId}")
    public Response getActivityById(@PathParam("activityId") String activityId) {
        if (activityId == null || activityId.length() < 4) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        Activity activity = activityRepository.findActivityById(activityId);

        if (activity == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        return Response.ok(activity).build();
    }

    @GET
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    @Path("{activityId}/user")
    public User getActivityUser(@PathParam("activityId") String activityId) {
        return activityRepository.findActivityById(activityId).getUser();
    }

    @POST
    @Path("activity")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public Response createActivity(MultivaluedMap<String, String> form) {
        Activity activity = new Activity();
        activity.setDescription(form.getFirst("description"));
        activity.setDuration(Integer.parseInt(form.getFirst("duration")));

        Activity created = activityRepository.createActivity(activity);

        return Response.ok(created).build();
    }

    @POST
    @Path("activity")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public Response createActivity(Activity activity) {
        Activity created = activityRepository.createActivity(activity);
        return Response.ok(created).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{activityId}")
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public Response updateActivity(@PathParam("activityId") String activityId, Activity activity) {
        return Response.ok(activityRepository.updateActivity(activityId, activity)).build();
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{activityId}")
    public Response deleteActivity(@PathParam("activityId") String activityId) {
        activityRepository.deleteActivity(activityId);
        return Response.ok().build();
    }
}
