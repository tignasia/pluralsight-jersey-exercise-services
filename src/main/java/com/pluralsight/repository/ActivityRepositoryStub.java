package com.pluralsight.repository;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.pluralsight.model.Activity;
import com.pluralsight.model.ActivitySearch;
import com.pluralsight.model.User;
import org.apache.commons.lang.math.RandomUtils;

import java.util.List;
import java.util.Map;

public class ActivityRepositoryStub implements ActivityRepository {
    private final static Map<String, Activity> store;
    private final static User stubUser;

    static {
        stubUser = createStubUser();

        Activity swimming = new Activity();
        swimming.setId("1234");
        swimming.setDescription("Swimming");
        swimming.setDuration(55);
        swimming.setUser(stubUser);

        Activity cycling = new Activity();
        cycling.setId("1300");
        cycling.setDescription("Cycling");
        cycling.setDuration(120);
        cycling.setUser(stubUser);

        Activity walking = new Activity();
        walking.setId("1001");
        walking.setDescription("Walking");
        walking.setDuration(120);
        walking.setUser(stubUser);

        Activity walkingADog = new Activity();
        walkingADog.setId("1002");
        walkingADog.setDescription("Walking a dog");
        walkingADog.setDuration(40);
        walkingADog.setUser(stubUser);

        Activity running = new Activity();
        running.setId("1003");
        running.setDescription("Running");
        running.setDuration(75);
        running.setUser(stubUser);

        store = ImmutableMap.of(
                swimming.getId(), swimming,
                cycling.getId(), cycling,
                walking.getId(), walking,
                walkingADog.getId(), walkingADog,
                running.getId(), running
        );
    }

    @Override
    public List<Activity> findAllActivities() {
        return ImmutableList.copyOf(store.values());
    }

    @Override
    public Activity findActivityById(String activityId) {
        return store.get(activityId);
    }

    @Override
    public Activity createActivity(Activity activity) {
        Activity created = new Activity();
        created.setId(String.valueOf(RandomUtils.nextInt(9000) + 1000));
        created.setDescription(activity.getDescription());
        created.setDuration(activity.getDuration());
        created.setUser(activity.getUser());
        return created;
    }

    @Override
    public Activity updateActivity(String activityId, Activity activity) {
        // look in db, update or create if not found
        return activity;
    }

    @Override
    public void deleteActivity(String activityId) {
        // look in db and delete
    }

    @Override
    public List<Activity> findByDescriptions(final List<String> descriptions) {
        Iterable<Activity> found = Iterables.filter(store.values(), new Predicate<Activity>() {
            @Override
            public boolean apply(Activity activity) {
                return descriptions.contains(activity.getDescription());
            }
        });

        return ImmutableList.copyOf(found);
    }

    @Override
    public List<Activity> findByCriteria(final ActivitySearch criteria) {
        Iterable<Activity> found = Iterables.filter(store.values(), new Predicate<Activity>() {
            @Override
            public boolean apply(Activity activity) {
                return criteria.getDescriptions().contains(activity.getDescription())
                        && (activity.getDuration() >= criteria.getDurationFrom())
                        && (activity.getDuration() <= criteria.getDurationTo());
            }
        });

        return ImmutableList.copyOf(found);
    }

    private static User createStubUser() {
        User user = new User();
        user.setId("42");
        user.setName("Arthur Dent");
        return user;
    }
}
