package com.pluralsight.repository;

import com.pluralsight.model.Activity;
import com.pluralsight.model.ActivitySearch;

import java.util.List;

public interface ActivityRepository {
    List<Activity> findAllActivities();

    Activity findActivityById(String activityId);

    Activity createActivity(Activity activity);

    Activity updateActivity(String activityId, Activity activity);

    void deleteActivity(String activityId);

    List<Activity> findByDescriptions(List<String> descriptions);

    List<Activity> findByCriteria(ActivitySearch criteria);
}
