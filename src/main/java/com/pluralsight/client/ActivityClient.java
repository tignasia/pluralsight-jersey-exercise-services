package com.pluralsight.client;

import com.pluralsight.model.Activity;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

public class ActivityClient {
    public static final String EXERCISE_SERVICES_URL = "http://localhost:8080/exercise-services/webapi/";
    public static final String ACTIVITIES_PATH = "activities";
    public static final String ACTIVITY_PATH = ACTIVITIES_PATH + "/activity";
    private Client client;

    public ActivityClient() {
        client = ClientBuilder.newClient();
    }

    public Activity getActivity(String id) {
        WebTarget target = client.target(EXERCISE_SERVICES_URL);

        // can use MediaType as parameter to request to select specific content type
        // e.g.:
        // String jsonResponse = target.path("activities/" + id).request(MediaType.APPLICATION_JSON).get(String.class);
        Response response = target.path(ACTIVITIES_PATH + "/" + id).request().get(Response.class);

        if (response.getStatus() != Response.Status.OK.getStatusCode()) {
            throw new RuntimeException("Error on server. Returned HTTP status: " + response.getStatus());
        }

        return response.readEntity(Activity.class);
    }

    public List<Activity> getActivities() {
        WebTarget target = client.target(EXERCISE_SERVICES_URL);
        // get(List.class) won't work!
        return target.path(ACTIVITIES_PATH).request().get(new GenericType<List<Activity>>() {});
    }

    public Activity createActivity(Activity activity) {
        WebTarget target = client.target(EXERCISE_SERVICES_URL);

        Response response = target.path(ACTIVITY_PATH)
                .request()
                .post(Entity.entity(activity, MediaType.APPLICATION_JSON));

        if (response.getStatus() != Response.Status.OK.getStatusCode()) {
            throw new RuntimeException("Error on server. Returned HTTP status: " + response.getStatus());
        }

        return response.readEntity(Activity.class);
    }

    public Activity updateActivity(Activity activity) {
        WebTarget target = client.target(EXERCISE_SERVICES_URL);

        Response response = target.path(ACTIVITIES_PATH + "/" + activity.getId())
                .request()
                .put(Entity.entity(activity, MediaType.APPLICATION_JSON));

        if (response.getStatus() != Response.Status.OK.getStatusCode()) {
            throw new RuntimeException("Error on server. Returned HTTP status: " + response.getStatus());
        }

        return response.readEntity(Activity.class);
    }

    public void deleteActivity(String activityId) {
        WebTarget target = client.target(EXERCISE_SERVICES_URL);

        Response response = target.path(ACTIVITIES_PATH + "/" + activityId)
                .request().delete();

        if (response.getStatus() != Response.Status.OK.getStatusCode()) {
            throw new RuntimeException("Error on server. Returned HTTP status: " + response.getStatus());
        }
    }
}
