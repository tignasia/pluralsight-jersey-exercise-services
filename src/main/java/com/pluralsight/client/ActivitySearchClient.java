package com.pluralsight.client;

import com.pluralsight.model.Activity;
import com.pluralsight.model.ActivitySearch;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.List;

public class ActivitySearchClient {
    public static final String EXERCISE_SERVICES_URL = "http://localhost:8080/exercise-services/webapi";
    public static final String ACTIVITIES_SEARCH_PATH = "search/activities";

    private Client client;

    public ActivitySearchClient() {
        client = ClientBuilder.newClient();
    }

    public List<Activity> search(String param, List<String> searchValues) {
        URI uri = UriBuilder.fromUri(EXERCISE_SERVICES_URL)
                .path(ACTIVITIES_SEARCH_PATH)
                .queryParam(param, searchValues.toArray())
                .build();

        WebTarget target = client.target(uri);

        List<Activity> activities = target.request(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<Activity>>() {
                });

        return activities;
    }

    public List<Activity> search(ActivitySearch criteria) {
        URI uri = UriBuilder.fromUri(EXERCISE_SERVICES_URL)
                .path(ACTIVITIES_SEARCH_PATH)
                .build();

        WebTarget target = client.target(uri);

        Response response = target.request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(criteria, MediaType.APPLICATION_JSON));

        return response.readEntity(new GenericType<List<Activity>>(){});
    }
}
