package com.pluralsight.client;

import com.pluralsight.model.Activity;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class ActivityClientTest {

    private ActivityClient client;

    @Before
    public void setUp() throws Exception {
        client = new ActivityClient();
    }

    @Test
    public void testGetActivity() throws Exception {
        Activity activity = client.getActivity("1234");

        assertNotNull(activity);
        System.out.println(activity.getDescription());
    }


    @Test(expected = RuntimeException.class)
    public void testThrowsExceptionWhenIdTooShort() throws Exception {
        client.getActivity("1");
        fail("RuntimeException expected.");
    }

    @Test
    public void testGetActivities() throws Exception {
        List<Activity> activities = client.getActivities();
        assertNotNull(activities);

        for (Activity a : activities) {
            System.out.println(a.getDescription());
        }
    }

    @Test
    public void testCreate() throws Exception {
        Activity swimming = new Activity();
        swimming.setDescription("Swimming");
        swimming.setDuration(90);

        Activity created = client.createActivity(swimming);
        assertNotNull(created);
        assertEquals(swimming.getDescription(), created.getDescription());
        assertEquals(swimming.getDuration(), created.getDuration());
        assertNotNull(created.getId());
    }

    @Test
    public void testPutActivity() throws Exception {
        Activity activity = new Activity();
        activity.setId("1313");
        activity.setDescription("Walking a dog");
        activity.setDuration(30);

        Activity updated = client.updateActivity(activity);
        assertNotNull(updated);
    }

    @Test
    public void testDeleteActivity() throws Exception {
        client.deleteActivity("1234");
    }
}
