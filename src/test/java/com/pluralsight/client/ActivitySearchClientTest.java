package com.pluralsight.client;

import com.google.common.collect.ImmutableList;
import com.pluralsight.model.Activity;
import com.pluralsight.model.ActivitySearch;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ActivitySearchClientTest {
    private ActivitySearchClient client;

    @Before
    public void setUp() throws Exception {
        client = new ActivitySearchClient();
    }

    @Test
    public void testSearchByDescription() throws Exception {
        ImmutableList<String> searchValues = ImmutableList.of("Running", "Walking");
        List<Activity> found = client.search("description", searchValues);
        assertNotNull(found);
        assertEquals(2, found.size());
    }

    @Test
    public void testSearchUsingSearchObject() throws Exception {
        ImmutableList<String> searchValues = ImmutableList.of("Walking", "Running");
        ActivitySearch criteria = new ActivitySearch();
        criteria.setDescriptions(searchValues);
        criteria.setDurationFrom(20);
        criteria.setDurationTo(100);
        List<Activity> found = client.search(criteria);
        assertNotNull(found);
        assertEquals(1, found.size());
    }
}